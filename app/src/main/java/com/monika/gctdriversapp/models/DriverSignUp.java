package com.monika.gctdriversapp.models;

/**
 * Created by DELL on 20/02/2017.
 */

public class DriverSignUp {
    String drivername;
    String emailid;
    String mobilenumber;
    String truckimage;
    String signupdrv;
    String password;
    String confirmpassword;

    public String getConfirmpassword() {
        return confirmpassword;
    }

    public void setConfirmpassword(String confirmpassword) {
        this.confirmpassword = confirmpassword;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


    public String getSignupdrv() {
        return signupdrv;
    }

    public void setSignupdrv(String signupdrv) {
        this.signupdrv = signupdrv;
    }



    public String getTruckimage() {
        return truckimage;
    }

    public void setTruckimage(String truckimage) {
        this.truckimage = truckimage;
    }

    public String getAgencyid() {
        return agencyid;
    }

    public void setAgencyid(String agencyid) {
        this.agencyid = agencyid;
    }

    public String getDrivername() {
        return drivername;
    }

    public void setDrivername(String drivername) {
        this.drivername = drivername;
    }

    public String getEmailid() {
        return emailid;
    }

    public void setEmailid(String emailid) {
        this.emailid = emailid;
    }

    public String getMobilenumber() {
        return mobilenumber;
    }

    public void setMobilenumber(String mobilenumber) {
        this.mobilenumber = mobilenumber;
    }

    String agencyid;
}
