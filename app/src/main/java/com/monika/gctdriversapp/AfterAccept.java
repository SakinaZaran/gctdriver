package com.monika.gctdriversapp;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.monika.gctdriversapp.location.GpsTrackerAlarmReceiver;
import com.monika.gctdriversapp.location.LocationService;

import java.util.UUID;

public class AfterAccept extends AppCompatActivity {


    private static final String TAG = "AfterAccept";
    TextView slocation, elocation;
    Button bslocation, beloaction;
    private String start, end;
    private boolean currentlyTracking;
    private AlarmManager alarmManager;
    private Intent gpsTrackerIntent;
    private PendingIntent pendingIntent;
    private int intervalInMinutes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_after_accept);
        slocation = (TextView) findViewById(R.id.start);
        elocation = (TextView) findViewById(R.id.endl);
        bslocation = (Button) findViewById(R.id.bslocation);
        beloaction = (Button) findViewById(R.id.belocation);
        if (getIntent().hasExtra("start")) {
            start = getIntent().getExtras().getString("start");
            end = getIntent().getExtras().getString("end");
            elocation.setText(end);
            slocation.setText(start);
        }
    }

    public void slocation(View view) {

        startService(new Intent(this, LocationService.class));
        trackLocation();
        Intent intent = new Intent(AfterAccept.this, ViewMap.class);
        startActivity(intent);


    }


    private boolean checkIfGooglePlayEnabled() {
        if (GooglePlayServicesUtil.isGooglePlayServicesAvailable(this) == ConnectionResult.SUCCESS) {
            return true;
        } else {
            Log.e(TAG, "unable to connect to google play services.");
            Toast.makeText(getApplicationContext(), "Google Play Services unavailable", Toast.LENGTH_LONG).show();
            return false;
        }
    }


    // called when trackingButton is tapped
    protected void trackLocation() {
        LocalStorage storage = new LocalStorage(this);


        storage.setInteger("intervalInMinutes", 1);

        if (!checkIfGooglePlayEnabled()) {
            return;
        }

        if (currentlyTracking) {
            cancelAlarmManager();
            Log.i("STATUS:", "Tracking " + String.valueOf(currentlyTracking));
            currentlyTracking = false;
            storage.setBoolean("currentlyTracking", false);
            storage.setString("sessionID", "");
        } else {
            startAlarmManager();

            currentlyTracking = true;
            storage.setBoolean("currentlyTracking", true);
            storage.setFloat("totalDistanceInMeters", 0f);
            storage.setBoolean("firstTimeGettingPosition", true);
            storage.setString("sessionID", UUID.randomUUID().toString());
        }


    }

    private void startAlarmManager() {
        Log.d(TAG, "startAlarmManager");

        Context context = getBaseContext();
        alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        gpsTrackerIntent = new Intent(context, GpsTrackerAlarmReceiver.class);
        pendingIntent = PendingIntent.getBroadcast(context, 0, gpsTrackerIntent, 0);

        LocalStorage storage = new LocalStorage(this);
        intervalInMinutes = storage.getInteger("intervalInMinutes");

        alarmManager.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP,
                SystemClock.elapsedRealtime(),
                intervalInMinutes * 60000, // 60000 = 1 minute
                pendingIntent);
    }

    private void cancelAlarmManager() {
        Log.d(TAG, "cancelAlarmManager");

        Context context = getBaseContext();
        Intent gpsTrackerIntent = new Intent(context, GpsTrackerAlarmReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, gpsTrackerIntent, 0);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.cancel(pendingIntent);
    }
}
