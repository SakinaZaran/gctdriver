package com.monika.gctdriversapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.monika.gctdriversapp.models.Order;

import java.util.ArrayList;

/**
 * Created by DELL on 12/03/2017.
 */

public class AllOrdersAdapter extends BaseAdapter {
    ListView lstAllOrders;
    Context context;
    ArrayList<Order> allOrders;

    public AllOrdersAdapter(ArrayList<Order> list, Context context) {
        super();
        this.allOrders=list;
        this.context=context;
    }

    @Override
    public int getCount() {
        return allOrders.size();
    }

    @Override
    public Object getItem(int position) {
        return allOrders.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null)
            convertView = ((LayoutInflater)
                    AllOrdersAdapter.this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.all_orders,null);
        TextView ConsumerName= (TextView) convertView.findViewById(R.id.cname);
        TextView StartLocation= (TextView) convertView.findViewById(R.id.sln);
        TextView EndLocation= (TextView) convertView.findViewById(R.id.eln);
        TextView tvDateForText= (TextView) convertView.findViewById(R.id.tvDateForText);
        ConsumerName.setText(allOrders.get(position).getConsumerName());
        StartLocation.setText(allOrders.get(position).getStartLocation());
        EndLocation.setText(allOrders.get(position).getEndLocation());
        tvDateForText.setText(allOrders.get(position).getDate());



        return convertView;
    }
}
