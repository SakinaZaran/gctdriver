package com.monika.gctdriversapp;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.monika.gctdriversapp.models.Driver;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class DriverActivity extends AppCompatActivity implements Serializable {
    Button Login;
    TextView forgotPassword, SignUp;
    EditText AgencyId, DriverId, Password;
    Context context;

    public Driver driver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_driver);

        Login = (Button) findViewById(R.id.Login);
        AgencyId = (EditText) findViewById(R.id.AgencyId);
        DriverId = (EditText) findViewById(R.id.DriverId);
        Password = (EditText) findViewById(R.id.Password);
        forgotPassword = (TextView) findViewById(R.id.forgotPassword);
        SignUp = (TextView) findViewById(R.id.SignUp);
        forgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(DriverActivity.this, ForgotPassword.class));
            }
        });


    }

    public void clicked(View view) {
       /* Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse("https://theaimtech.com"));
        startActivity(i);*/




        StringRequest jsonObjReq = new StringRequest(Request.Method.POST,
                ServerConstants.LOGIN_URL,
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        Log.d("RESPONSE", response.toString());
                        if (response.trim().equals("false")) {
                            Toast.makeText(DriverActivity.this, "Either Username or password is wrong", Toast.LENGTH_SHORT).show();
                        } else {
                            try {

                                JSONObject obj = new JSONObject(response);
                                JSONArray arra = obj.getJSONArray("driver");
                                JSONObject userObj = arra.getJSONObject(0);
                                driver = new Driver();
                                driver.setName(userObj.get("Name").toString());
                                driver.setLatitude(userObj.get("Latitude").toString());
                                driver.setLongitude(userObj.get("Longitude").toString());
                                driver.setPassword(userObj.get("Password").toString());
                                driver.setImage(userObj.get("Image").toString());
                                driver.setTrucknumber(userObj.get("Truck_No").toString());
                                driver.setId(userObj.get("Id").toString());
                                driver.setMobilenumber(userObj.get("Mobile_No").toString());

                                new LocalStorage(DriverActivity.this).setString("driver", Utils.objectToString(driver));
                                Intent i = new Intent(DriverActivity.this, Dashboard.class);
                                Toast.makeText(DriverActivity.this, "Login Successful for " + driver.getName() , Toast.LENGTH_LONG).show();

                                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(i);
                                finish();
                            } catch (JSONException ex) {

                            }
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("RESPONSE", "Error: " + error.getMessage());
                Toast.makeText(DriverActivity.this, error.toString(), Toast.LENGTH_SHORT).show();

            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Id", DriverId.getText().toString().trim());
                params.put("Password", Password.getText().toString().trim());
                params.put("AgencyId", AgencyId.getText().toString().trim());


                return params;
            }

        };
// Adding request to request queue
        Volley.newRequestQueue(DriverActivity.this).add(jsonObjReq);
    }

    public void clickon(View view) {
        startActivity(new Intent(DriverActivity.this, ForgotPassword.class));
    }

    public void clickhere(View view) {
        startActivity(new Intent(DriverActivity.this, SignUp.class));
    }
}
