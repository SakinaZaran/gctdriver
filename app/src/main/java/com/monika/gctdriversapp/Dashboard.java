package com.monika.gctdriversapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.monika.gctdriversapp.listdrivers.List_of_Orders;

public class Dashboard extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
    }

    public void clickhere(View view) {
        Intent intent=new Intent(Dashboard.this, List_of_Orders.class);
        startActivity(intent);
    }

    public void clickhere1(View view) {
        Intent intent=new Intent(Dashboard.this,AllOrders.class);
        startActivity(intent);
    }

    public void ClickedCurrentOrders(View view) {
        Intent intent=new Intent(Dashboard.this,CurrentOrders.class);
        startActivity(intent);
    }

    public void showAbout(View view) {
        Intent intent=new Intent(Dashboard.this,About.class);
        startActivity(intent);
    }



    public void logout(View view) {
        Intent intent=new Intent(Dashboard.this,DriverActivity.class);
        startActivity(intent);
    }


    public void dprofile(View view) {
         Intent intent=new Intent(Dashboard.this,Driver_Profile.class);
        startActivity(intent);
    }
}
