package com.monika.gctdriversapp.listdrivers;

/**
 * Created by Moni on 22/01/2017.
 */
public class ItemAllOrders {
    String ConsumerName;
    String StartLocation;
    String EndLocation;

    public ItemAllOrders(String ConsumerName,String StartLocation,String EndLocation){
        super();
        this.ConsumerName=ConsumerName;
        this.StartLocation=StartLocation;
        this.EndLocation=EndLocation;
    }

    public String getConsumerName(){return ConsumerName;}

    public String getStartLocation(){return StartLocation;}

    public String getEndLocation(){return EndLocation;}
}
