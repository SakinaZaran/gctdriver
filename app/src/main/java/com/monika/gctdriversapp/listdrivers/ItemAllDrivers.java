package com.monika.gctdriversapp.listdrivers;

/**
 * Created by moni on 1/3/2017.
 */

public class ItemAllDrivers {
    String Name;
    String MobileNumber;
    String Email;

    public ItemAllDrivers(String Name, String MobileNumber, String Email){
        super();
        this.Name=Name;
        this.MobileNumber=MobileNumber;
        this.Email=Email;

    }
    public String getName(){ return  Name ;}

    public String getMobileNumber(){ return MobileNumber;}

    public  String getEmail() { return Email;}
}
