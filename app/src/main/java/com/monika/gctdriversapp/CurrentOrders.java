package com.monika.gctdriversapp;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.monika.gctdriversapp.models.Driver;
import com.monika.gctdriversapp.models.Order;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class CurrentOrders extends AppCompatActivity {
    ListView listCurrentOrders;
    ArrayList<Order> currentOrders;
    Driver currentDriver;
    Context context;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_of_orders);
        currentDriver = (Driver) Utils.stringToObject(new LocalStorage(this).getString("driver"));
        listCurrentOrders= (ListView) findViewById(R.id.pendingOrders);
        currentOrders=new ArrayList<>();
        final CurrentOrdersAdapter adapter=new CurrentOrdersAdapter(currentOrders,this);
        listCurrentOrders.setAdapter(adapter);
        StringRequest jsonObjReq = new StringRequest(Request.Method.POST,
                ServerConstants.CURRENT_ORDERS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("CurrentOrdersRESP", response.toString());
                        try {
                            if (response.toUpperCase().contains("Driver_Id".toUpperCase())) {
                                JSONObject agency = new JSONObject(response);
                                JSONArray agencies = agency.getJSONArray("orders");
                                for (int i = 0; i < agencies.length(); i++) {
                                    Order myAgency = new Order();
                                    JSONObject loggedinAgency = agencies.getJSONObject(i);
                                    myAgency.setConsumerName(loggedinAgency.getString("name"));
                                    myAgency.setStartLocation(loggedinAgency.getString("Start_location"));
                                    myAgency.setEndLocation(loggedinAgency.getString("End_location"));
                                    currentOrders.add(myAgency);
                                }

                                adapter.notifyDataSetChanged();

                            } else {
                                Toast.makeText(CurrentOrders.this, "Something went wrong" + response, Toast.LENGTH_SHORT).show();
                            }

                        } catch (Exception ex) {
                        }
                    }

                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }

        ) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("driverid", currentDriver.getId());
                Log.d("PARAMETERS:", params.toString());
                return params;
            }
        };
        Volley.newRequestQueue(CurrentOrders.this).add(jsonObjReq);
    }


}

