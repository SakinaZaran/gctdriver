package com.monika.gctdriversapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.monika.gctdriversapp.models.DriverSignUp;

import java.util.HashMap;
import java.util.Map;

public class SignUp extends AppCompatActivity {
    EditText etdrivername;
    EditText etemailid;
    EditText etmobilenumber;
    EditText etagencyid;
    EditText etpassword;
    EditText etconfirmpassword;
    Button btnsignupdrv;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        etdrivername= (EditText) findViewById(R.id.drivername);
        etemailid= (EditText) findViewById(R.id.emailid);
        etmobilenumber= (EditText) findViewById(R.id.mobilenum);
        etagencyid= (EditText) findViewById(R.id.agencyid);
        etpassword= (EditText) findViewById(R.id.pwd);
        etconfirmpassword= (EditText) findViewById(R.id.cpwd);
        btnsignupdrv= (Button) findViewById(R.id.signupdrv);
        driverSignUp = new DriverSignUp();
    }
    DriverSignUp driverSignUp;

    public void signup(View view) {
        Toast.makeText(this, "Clicked", Toast.LENGTH_LONG).show();
        if (TextUtils.isEmpty(etdrivername.getText().toString().trim())) {
            etdrivername.setError("please enter drivername");
            return;
        }
        if (TextUtils.isEmpty(etemailid.getText().toString().trim())) {
            etemailid.setError("please enter emailid");
            return;
        }
        if (TextUtils.isEmpty(etmobilenumber.getText().toString().trim())) {
            etmobilenumber.setError("please enter mobileNo");
            return;
        }
        if (TextUtils.isEmpty(etagencyid.getText().toString().trim())) {
            etagencyid.setError("please enter agencyId");
            return;
        }
        if (TextUtils.isEmpty(etpassword.getText().toString().trim())) {
            etpassword.setError("please enter password");
            return;
        }
        if (TextUtils.isEmpty(etconfirmpassword.getText().toString().trim())) {
            etconfirmpassword.setError("please enter confirmpassword");
            return;
        }
        StringRequest jsonObjReq = new StringRequest(Request.Method.POST,
                ServerConstants.DRIVERSIGNUP_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("RESPONSE", response.toString());
                        try {
                            if (response.equals("success")) {


                                Intent i = new Intent(SignUp.this, MainActivity.class);
                                Toast.makeText(SignUp.this, "Login thayu " + driverSignUp.getDrivername() + " nu", Toast.LENGTH_LONG).show();

                                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(i);
                                finish();
                            } else {
                                Toast.makeText(SignUp.this, "Something went wrong" + response, Toast.LENGTH_SHORT).show();
                            }

                        } catch (Exception ex) {
                        }
                    }

                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }

        ) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("name", etdrivername.getText().toString().trim());
                params.put("agencyid", etagencyid.getText().toString().trim());
                params.put("mobilenumber", etmobilenumber.getText().toString().trim());
                params.put("emailid", etemailid.getText().toString().trim());
                params.put("password", etpassword.getText().toString().trim());

                return params;
            }
        };
        Volley.newRequestQueue(SignUp.this).add(jsonObjReq);
    }



    }

