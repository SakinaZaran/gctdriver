package com.monika.gctdriversapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class AfterStatusClick extends AppCompatActivity {
String consumername,start,end;
    TextView cname,slocation,elocation;
    Button btaccept;
    private String id;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_after_status_click);
        cname= (TextView) findViewById(R.id.tvc_name);
        slocation= (TextView) findViewById(R.id.tvc_startlocation);
        elocation= (TextView) findViewById(R.id.tvc_endlocation);
        btaccept= (Button) findViewById(R.id.btnaccept);

        if(getIntent().hasExtra("start")){
            consumername=getIntent().getExtras().getString("conname");
            start=getIntent().getExtras().getString("start");
            end=getIntent().getExtras().getString("end");
            id=getIntent().getExtras().getString("id");
            cname.setText(consumername);
            elocation.setText(end);
            slocation.setText(start);
            AppController.getInstance().currentOrder.setId(id);
        }
    }

    public void accept(View view) {
        LocalStorage storage = new LocalStorage(this);
        storage.setString("orderid",id);
        Intent intent=new Intent(AfterStatusClick.this,AfterAccept.class);
        intent.putExtra("start",start);
        intent.putExtra("end",end);

        startActivity(intent);
    }
}
