package com.monika.gctdriversapp;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Monika Patel on 03-02-2016.
 */
public class LocalStorage {
    Context context;
    SharedPreferences prefs;

    public LocalStorage(Context context) {
        this.context = context;
         prefs=context.getSharedPreferences("in.gct.LocalData", Context.MODE_PRIVATE);
    }
    public void setString(String key, String value)
    {
        SharedPreferences.Editor editor= prefs.edit();
        editor.putString(key, value);
        editor.commit();
    }
    public void setInteger(String key, int value)
    {
        SharedPreferences.Editor editor= prefs.edit();
        editor.putInt(key, value);
        editor.commit();
    }
    public void setBoolean(String key, boolean value)
    {
        SharedPreferences.Editor editor= prefs.edit();
        editor.putBoolean(key, value);
        editor.commit();
    }
   public void clearData()
   {
       SharedPreferences.Editor editor= prefs.edit();
       editor.clear();
       editor.commit();
   }
    public  boolean getBoolean(String key)
    {
        return  prefs.getBoolean(key,false);
    }
    public String getString(String key)
    {
        return  prefs.getString(key,"");
    }
    public  int getInteger(String key)
    {
        return  prefs.getInt(key,1);
    }
   public float getFloat(String key)
    {
        return  prefs.getFloat(key,0.f);
    }
   public void setFloat(String key, float Value)
    {
        SharedPreferences.Editor editor= prefs.edit();
        editor.putFloat(key,Value);
        editor.commit();
    }
}
