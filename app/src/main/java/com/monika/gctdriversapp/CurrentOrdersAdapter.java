package com.monika.gctdriversapp;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.monika.gctdriversapp.models.Order;

import java.util.ArrayList;

/**
 * Created by DELL on 12/03/2017.
 */

public class CurrentOrdersAdapter extends BaseAdapter {
    ArrayList<Order> currentOrders;
    ListView listCurrentOrders;
    Activity context;

    public CurrentOrdersAdapter(ArrayList<Order> list, Activity context) {
        super();
        this.currentOrders=list;
        this.context=context;
    }

    @Override
    public int getCount() {
        return currentOrders.size();
    }

    @Override
    public Object getItem(int position) {
        return currentOrders.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        if (convertView == null)
            convertView = ((LayoutInflater)
                    CurrentOrdersAdapter.this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.activity_current_orders,null);
        TextView ConsumerName= (TextView) convertView.findViewById(R.id.tvc_name);
        TextView StartLocation= (TextView) convertView.findViewById(R.id.tvc_startlocation);
        TextView EndLocation= (TextView) convertView.findViewById(R.id.tvc_endlocation);
        Button btnComplete= (Button) convertView.findViewById(R.id.btnComplete);
        Button btnCancel= (Button) convertView.findViewById(R.id.btnCancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               currentOrders.remove(position);
                CurrentOrdersAdapter.this.notifyDataSetChanged();
                if(currentOrders.size()<=0){
                    context.finish();
                }
            }
        });
        btnComplete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, "Order Completed", Toast.LENGTH_SHORT).show();
            }
        });
        ConsumerName.setText(currentOrders.get(position).getConsumerName());
        StartLocation.setText(currentOrders.get(position).getStartLocation());
        EndLocation.setText(currentOrders.get(position).getEndLocation());
        return convertView;


    }
}
