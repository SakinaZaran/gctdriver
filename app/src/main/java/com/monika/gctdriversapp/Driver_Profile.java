package com.monika.gctdriversapp;

import android.content.Intent;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

public class Driver_Profile extends AppCompatActivity {
    ImageView driverimage;
    TextView profilepic;
    EditText etdrivername,etemail, etmobileno ,etagencyid;
    Button btnsubmit;
     LocalStorage Storage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_driver_profile);

        driverimage= (ImageView) findViewById(R.id.imgdriver);
        profilepic= (TextView) findViewById(R.id.prfpic);
        etdrivername= (EditText) findViewById(R.id.edname);
        etemail= (EditText) findViewById(R.id.eemail);
        etmobileno= (EditText) findViewById(R.id.emno);
        etagencyid= (EditText) findViewById(R.id.eagencyid);
        btnsubmit= (Button) findViewById(R.id.btsubmit);

        Storage=new LocalStorage(this);


    }

    public void takeimg(View view) {
        Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
        startActivity(intent);

        Intent intent1=new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivity(intent1);

    }
}
