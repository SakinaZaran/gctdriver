package com.monika.gctdriversapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.monika.gctdriversapp.listdrivers.List_of_Orders;
import com.monika.gctdriversapp.models.ChangePassword;
import com.monika.gctdriversapp.models.Order;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.monika.gctdriversapp.R.id.pendingOrders;

public class ChangePass extends AppCompatActivity {
    EditText currentpassword,newpassword,confirmpassword;
    ChangePassword changePassword;
    Button submit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_pass);
        currentpassword = (EditText) findViewById(R.id.cpass);
        newpassword = (EditText) findViewById(R.id.npass);
        confirmpassword = (EditText) findViewById(R.id.conpass);
        submit= (Button) findViewById(R.id.btnsubmit);


        StringRequest jsonObjReq = new StringRequest(Request.Method.POST,
                ServerConstants.CHANGE_PASSWORD,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("RESPONSE", response.toString());
                        try {
                            if (!response.contains("false")) {
                                JSONObject agency = new JSONObject(response);
                                JSONArray agencies = agency.getJSONArray("orders");
                                for (int i = 0; i < agencies.length(); i++) {
                                    ChangePassword myAgency = new ChangePassword();
                                    JSONObject loggedinAgency = agencies.getJSONObject(i);
                                    myAgency.setNewpassword(loggedinAgency.getString("name"));
                                    myAgency.setCurrentpassword(loggedinAgency.getString("Start_location"));
                                    myAgency.setConfirmpassword(loggedinAgency.getString("End_location"));
                                   // pendingOrders.add(myAgency);
                                }

                                //adapter.notifyDataSetChanged();

                            } else {
                                Toast.makeText(ChangePass.this, "Something went wrong" + response, Toast.LENGTH_SHORT).show();
                            }

                        } catch (Exception ex) {
                        }
                    }

                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }

        ) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("driverid", changePassword.getId());
                Log.d("PARAMETERS:", params.toString());
                return params;
            }
        };
        Volley.newRequestQueue(ChangePass.this).add(jsonObjReq);
    }
    }

