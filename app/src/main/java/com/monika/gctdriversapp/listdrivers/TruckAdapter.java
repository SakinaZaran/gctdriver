package com.monika.gctdriversapp.listdrivers;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.monika.gctdriversapp.R;

import java.util.List;

/**
 * Created by Moni on 08/01/2017.
 */
public class TruckAdapter extends BaseAdapter {
    String DisplayType;
    Context context;
    protected LayoutInflater inflater;
    List<ItemAllTrucks> allTrucks;



    public  TruckAdapter(List<ItemAllTrucks> list,Context context){
        super();
        this.allTrucks=list;
        this.context=context;


    }
    @Override
    public int getCount() { return allTrucks.size(); }

    @Override
    public Object getItem(int position) {
        return allTrucks.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {


        if (convertView == null)
            convertView = ((LayoutInflater)
            context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.single_list_item_truck,null);

        TextView Name= (TextView) convertView.findViewById(R.id.DriverName);
        TextView MobileNumber= (TextView) convertView.findViewById(R.id.DriverNumber);
        Name.setText(allTrucks.get(position).getName());
        MobileNumber.setText(allTrucks.get(position).getMobileNumber());
        return convertView;
    }
}
