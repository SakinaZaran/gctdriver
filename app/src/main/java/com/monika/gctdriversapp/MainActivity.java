package com.monika.gctdriversapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.widget.ListView;

import com.monika.gctdriversapp.listdrivers.ItemAllTrucks;
import com.monika.gctdriversapp.listdrivers.TruckAdapter;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    ListView listView;

    @Override
    public void onCreate (Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        listView= (ListView) findViewById(R.id.lst_view);
        ArrayList<ItemAllTrucks> Trucks =new ArrayList<>();
        Trucks.add(new ItemAllTrucks("Ramlal","1234"));
        Trucks.add(new ItemAllTrucks("Shamlal","4567"));
        Trucks.add(new ItemAllTrucks("Dayalal","3567"));
        Trucks.add(new ItemAllTrucks("Gandalal","8567"));
        Trucks.add(new ItemAllTrucks("Popatlal","7567"));
        Trucks.add(new ItemAllTrucks("Sitaram","3567"));
        Trucks.add(new ItemAllTrucks("Shamlal","1367"));
        Trucks.add(new ItemAllTrucks("lalaRam","8967"));
        TruckAdapter truckAdapter=new TruckAdapter(Trucks,getApplicationContext());
        listView.setAdapter(truckAdapter);
        truckAdapter.notifyDataSetInvalidated();


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }
}
