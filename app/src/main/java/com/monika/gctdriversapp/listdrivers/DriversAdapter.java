package com.monika.gctdriversapp.listdrivers;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.monika.gctdriversapp.R;

import java.util.List;

/**
 * Created by moni on 1/2/2017.
 */

public class DriversAdapter extends BaseAdapter {
    String DisplayType;
    Context context;
    protected LayoutInflater inflater;
    List<ItemAllDrivers> allDrivers;


    public DriversAdapter(List<ItemAllDrivers> list,Context context){
        super();
        this.allDrivers=list;
        this.context=context;


    }

    @Override
    public int getCount() {
        return allDrivers.size();
    }

    @Override
    public Object getItem(int position) {
        return allDrivers.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null)
            convertView = ((LayoutInflater)
                    context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.driver_single_list_item,null);

        TextView DriverName= (TextView) convertView.findViewById(R.id.drvname);
        TextView DriverNumber= (TextView) convertView.findViewById(R.id.drvNumber);
        TextView Email= (TextView) convertView.findViewById(R.id.Email);
        DriverName.setText(allDrivers.get(position).getName());
        DriverNumber.setText(allDrivers.get(position).getMobileNumber());
        Email.setText(allDrivers.get(position).getEmail());
        return convertView;



    }
}
