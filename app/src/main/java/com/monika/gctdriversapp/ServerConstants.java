package com.monika.gctdriversapp;

/**
 * Created by Monika Patel on 18/02/2017.
 */

public class ServerConstants {
    public static String BASE_URL="http://35.162.97.17/gct/";
    public static   String  LOGIN_URL=BASE_URL+"Dlogin.php";
    public  static String DRIVERSIGNUP_URL=BASE_URL+"I_Driver.php";
    public  static String PENDING_ORDERS=BASE_URL+"getPendingOrders.php";
    public  static String ALL_ORDERS=BASE_URL+"getAllOrders.php";
    public  static String CURRENT_ORDERS=BASE_URL+"getCurrentOrders.php";
    public  static String CHANGE_PASSWORD=BASE_URL+"changepassword.php";
    public  static String UPDATE_LOCATION=BASE_URL+"location.php";
}
