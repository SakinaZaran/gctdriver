package com.monika.gctdriversapp.listdrivers;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.monika.gctdriversapp.R;

import java.util.ArrayList;


/**
 * Created by moni on 1/2/2017.
 */

public class FragmentActivityMain extends Fragment {
    ListView listView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View myView = inflater.inflate(R.layout.list_common, null);
        listView = (ListView) myView.findViewById(R.id.list_view);
        return myView;


    }

    @Override
    public void onStart() {
        super.onStart();
        ArrayList<ItemAllDrivers> drivers = new ArrayList<>();
        drivers.add(new ItemAllDrivers("Ramlal", "1274474", "viraj@gmail.com"));
        drivers.add(new ItemAllDrivers("jigar", "1274474", "viraj@gmail.com"));
        drivers.add(new ItemAllDrivers("Arif", "1274474", "viraj@gmail.com"));
        drivers.add(new ItemAllDrivers("Gaurang", "1274474", "viraj@gmail.com"));
        DriversAdapter driversAdapter = new DriversAdapter(drivers,getContext());
        listView.setAdapter(driversAdapter);
        driversAdapter.notifyDataSetInvalidated();

    }
}
