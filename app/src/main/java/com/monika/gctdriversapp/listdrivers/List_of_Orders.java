package com.monika.gctdriversapp.listdrivers;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.monika.gctdriversapp.LocalStorage;
import com.monika.gctdriversapp.PendingOrdersAdapter;
import com.monika.gctdriversapp.R;
import com.monika.gctdriversapp.ServerConstants;
import com.monika.gctdriversapp.Utils;
import com.monika.gctdriversapp.models.Driver;
import com.monika.gctdriversapp.models.Order;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class List_of_Orders extends AppCompatActivity {
    ListView lstPendingOrders;
    ArrayList<Order> pendingOrders;
    Driver currentDriver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        currentDriver = (Driver) Utils.stringToObject(new LocalStorage(this).getString("driver"));
        setContentView(R.layout.activity_list_of_orders);
        lstPendingOrders = (ListView) findViewById(R.id.pendingOrders);
        pendingOrders = new ArrayList<>();
        final PendingOrdersAdapter adapter = new PendingOrdersAdapter(pendingOrders,this);
        lstPendingOrders.setAdapter(adapter);
        StringRequest jsonObjReq = new StringRequest(Request.Method.POST,
                ServerConstants.PENDING_ORDERS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("RESPONSE", response.toString());
                        try {
                            if (!response.contains("false")) {
                                JSONObject agency = new JSONObject(response);
                                JSONArray agencies = agency.getJSONArray("orders");
                                for (int i = 0; i < agencies.length(); i++) {
                                    Order order = new Order();
                                    JSONObject objOrder = agencies.getJSONObject(i);
                                    order.setConsumerName(objOrder.getString("name"));
                                    order.setStartLocation(objOrder.getString("Start_location"));
                                    order.setEndLocation(objOrder.getString("End_location"));
                                    order.setStartLat(objOrder.getString("start_lat"));
                                    order.setStartLong(objOrder.getString("start_long"));
                                    order.setEndLat(objOrder.getString("end_lat"));
                                    order.setEndLong(objOrder.getString("end_long"));
                                    order.setId(objOrder.getString("Id"));
                                    pendingOrders.add(order);
                                }

                                adapter.notifyDataSetChanged();

                            } else {
                                Toast.makeText(List_of_Orders.this, "Something went wrong" + response, Toast.LENGTH_SHORT).show();
                            }

                        } catch (Exception ex) {
                        }
                    }

                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }

        ) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("driverid", currentDriver.getId());
                Log.d("PARAMETERS:", params.toString());
                return params;
            }
        };
        Volley.newRequestQueue(List_of_Orders.this).add(jsonObjReq);
    }


}
