package com.monika.gctdriversapp;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.monika.gctdriversapp.models.Order;

import java.util.ArrayList;

/**
 * Created by DELL on 12/03/2017.
 */

public class PendingOrdersAdapter extends BaseAdapter {
    ListView lstPendingOrders;
    Context context;
    ArrayList<Order> pendingOrders;

    public PendingOrdersAdapter(ArrayList<Order> list, Context context) {
        super();
        this.pendingOrders = list;
        this.context = context;
    }

    @Override
    public int getCount() {
        return pendingOrders.size();
    }

    @Override
    public Object getItem(int position) {
        return pendingOrders.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (convertView == null)
            convertView = ((LayoutInflater)
                    PendingOrdersAdapter.this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.list_item_pending_orders, null);


        TextView ConsumerName = (TextView) convertView.findViewById(R.id.cnsmname);
        TextView StartLocation = (TextView) convertView.findViewById(R.id.slocation);
        TextView EndLocation = (TextView) convertView.findViewById(R.id.elocation);
        TextView Status = (TextView) convertView.findViewById(R.id.status);
        ConsumerName.setText(pendingOrders.get(position).getConsumerName());
        StartLocation.setText(pendingOrders.get(position).getStartLocation());
        EndLocation.setText(pendingOrders.get(position).getEndLocation());
        Status.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, AfterStatusClick.class);
                intent.putExtra("conname",pendingOrders.get(position).getConsumerName());
                intent.putExtra("start",pendingOrders.get(position).getStartLocation());
                intent.putExtra("end",pendingOrders.get(position).getEndLocation());
                intent.putExtra("id",pendingOrders.get(position).getId());
                AppController.getInstance().currentOrder=pendingOrders.get(position);
                context.startActivity(intent);
            }
        });
        return convertView;

    }


}
