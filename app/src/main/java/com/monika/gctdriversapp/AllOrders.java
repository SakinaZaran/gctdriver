package com.monika.gctdriversapp;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.monika.gctdriversapp.models.Driver;
import com.monika.gctdriversapp.models.Order;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class AllOrders extends AppCompatActivity {
    ListView listAllOrders;
    ArrayList<Order> allOrders;
    Driver currentDriver;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        currentDriver = (Driver) Utils.stringToObject(new LocalStorage(this).getString("driver"));
        setContentView(R.layout.activity_list_of_orders);
        listAllOrders = (ListView) findViewById(R.id.pendingOrders);
        allOrders = new ArrayList<>();
        final AllOrdersAdapter adapter = new AllOrdersAdapter(allOrders, this);
        listAllOrders.setAdapter(adapter);
        StringRequest jsonObjReq = new StringRequest(Request.Method.POST,
                ServerConstants.ALL_ORDERS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("RESPONSE", response.toString());
                        try {
                            if (response.toLowerCase().contains("driver_id".toLowerCase())) {
                                JSONObject agency = new JSONObject(response);
                                JSONArray agencies = agency.getJSONArray("orders");
                                for (int i = 0; i < agencies.length(); i++) {
                                    Order myAgency = new Order();
                                    JSONObject loggedinAgency = agencies.getJSONObject(i);
                                    myAgency.setConsumerName(loggedinAgency.getString("name"));
                                    myAgency.setStartLocation(loggedinAgency.getString("Start_location"));
                                    myAgency.setEndLocation(loggedinAgency.getString("End_location"));
                                    myAgency.setDate(loggedinAgency.getString("order_date"));
                                    allOrders.add(myAgency);
                                }

                                adapter.notifyDataSetChanged();

                            } else {
                                Toast.makeText(AllOrders.this, "Something went wrong" + response, Toast.LENGTH_SHORT).show();
                            }
                            if (allOrders.isEmpty()) {
                                listAllOrders.setVisibility(View.GONE);
                                findViewById(R.id.notdata).setVisibility(View.VISIBLE);
                            }
                        } catch (Exception ex) {
                        }
                    }

                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }

        ) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("driverid", currentDriver.getId());
                Log.d("PARAMETERS:", params.toString());
                return params;
            }
        };
        Volley.newRequestQueue(AllOrders.this).add(jsonObjReq);
    }


}

