package com.monika.gctdriversapp.location;

import android.app.Service;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.monika.gctdriversapp.AppController;
import com.monika.gctdriversapp.LocalStorage;
import com.monika.gctdriversapp.ServerConstants;

import java.util.HashMap;
import java.util.Map;


public class LocationService extends Service implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener {
    public static final int INTERVAL = 60000;
    GoogleApiClient mGoogleApiClient;
    LocalStorage storage;

    private LocationRequest mLocationRequest;

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        buildGoogleApiClient();
        return START_NOT_STICKY;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        storage = new LocalStorage(this);
    }


    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Toast.makeText(this, "Location Service Started", Toast.LENGTH_SHORT).show();
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(INTERVAL);
        mLocationRequest.setFastestInterval(INTERVAL);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        if (ContextCompat.checkSelfPermission(this,
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED && mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }

    }


    @Override
    public void onConnectionSuspended(int i) {
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
    }

    @Override
    public void onLocationChanged(final Location location) {
        if (null!= AppController.getInstance().currentOrder&&!TextUtils.isEmpty(AppController.getInstance().currentOrder.getId())) {
            try {

                StringRequest jsonObjReq = new StringRequest(Request.Method.POST,
                        ServerConstants.UPDATE_LOCATION,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                Log.d("RESPONSE", response.toString());
                                try {
                                    if (response.toUpperCase().contains("success".toUpperCase())) {
                                        Toast.makeText(LocationService.this, "Location Updated", Toast.LENGTH_SHORT).show();

                                    } else {
                                        Toast.makeText(LocationService.this, "Something went wrong" + response, Toast.LENGTH_SHORT).show();
                                    }

                                } catch (Exception ex) {
                                }
                            }

                        }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }) {
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        HashMap<String, String> parameters = new HashMap<>();
                        parameters.put("lat", Double.toString(location.getLatitude()));
                        parameters.put("long", Double.toString(location.getLongitude()));
                        parameters.put("id", storage.getString("orderid"));
                        return parameters;
                    }
                };
                Volley.newRequestQueue(LocationService.this).add(jsonObjReq);


            } catch (Exception ex) {
                Log.d("onLocationChanged(): ", ex.getMessage());
            }
        }
    }
}